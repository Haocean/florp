#include "florp/game/BehaviourLayer.h"
#include "florp/game/SceneManager.h"
#include "florp/game/IBehaviour.h"

namespace florp::game {
	
	void BehaviourLayer::Update() {
		CurrentRegistry().view<BehaviourBinding>().each([](auto entity, BehaviourBinding& binding) {
			for (std::shared_ptr<IBehaviour> b : binding.Behaviours) {
				if (b->Enabled) b->Update(entity);
			}
		});
	}

	void BehaviourLayer::LateUpdate() {
		CurrentRegistry().view<BehaviourBinding>().each([](auto entity, BehaviourBinding& binding) {
			for (std::shared_ptr<IBehaviour> b : binding.Behaviours) {
				if (b->Enabled) b->LateUpdate(entity);
			}
		});
	}

	void BehaviourLayer::FixedUpdate()
	{
		CurrentRegistry().view<BehaviourBinding>().each([](auto entity, BehaviourBinding& binding) {
			for (std::shared_ptr<IBehaviour> b : binding.Behaviours) {
				if (b->Enabled) b->FixedUpdate(entity);
			}
		});
	}

	void BehaviourLayer::RenderGUI() {
		CurrentRegistry().view<BehaviourBinding>().each([](auto entity, BehaviourBinding& binding) {
			for (std::shared_ptr<IBehaviour> b : binding.Behaviours) {
				if (b->Enabled) b->RenderGUI(entity);
			}
		});
	}

	void BehaviourLayer::OnSceneEnter() {
		CurrentRegistry().view<BehaviourBinding>().each([](auto entity, BehaviourBinding& binding) {
			for (std::shared_ptr<IBehaviour> b : binding.Behaviours) {
				if (b->Enabled) b->OnLoad(entity);
			}
		});
	}

	void BehaviourLayer::OnSceneExit() {
		CurrentRegistry().view<BehaviourBinding>().each([](auto entity, BehaviourBinding& binding) {
			for (std::shared_ptr<IBehaviour> b : binding.Behaviours) {
				if (b->Enabled) b->OnUnload(entity);
			}
		});
	}
}
